export const registerSystemSettings = function() {

  /**
   * Track the system version upon which point a migration was last applied
   */
  game.settings.register("mta", "systemMigrationVersion", {
    name: "System Migration Version",
    scope: "world",
    config: false,
    type: String,
    default: ""
  });
  
  /**
   * Option to automatically collapse Item Card descriptions
   */
  game.settings.register("mta", "autoCollapseItemDescription", {
    name: "Collapse Item Cards in Chat",
    hint: "Automatically collapse Item Card descriptions in the Chat Log",
    scope: "client",
    config: true,
    default: true,
    type: Boolean,
    onChange: s => {
      ui.chat.render();
    }
  });
};

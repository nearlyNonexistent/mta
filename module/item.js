import {
  DiceRollerDialogue
} from "./dialogue-diceRoller.js";
/**
 * Override and extend the basic :class:`Item` implementation
 */
export class ItemMtA extends Item {
  
  /* -------------------------------------------- */
  /*	Data Preparation														*/
  /* -------------------------------------------- */

  /**
   * Augment the basic Item data model with additional dynamic data.
   */
  prepareData() {
    super.prepareData(); //TODO: Put this functionality in where the item is initialised to avoid problems

    if (!this.data.img || this.data.img === CONST.DEFAULT_TOKEN || this.data.img.startsWith('systems/mta/icons/placeholders')){
    
      let img = 'systems/mta/icons/placeholders/item-placeholder.svg';
      let type = this.data.type;
      if(type === "melee"){
        if(this.data.data.weaponType === "Unarmed") type = "unarmed";
        else if(this.data.data.weaponType === "Thrown") type = "thrown";
      }
      else if(type === "tilt"){
        if(this.data.data.isEnvironmental) type = "environmental";
      }
      else if(this.data.type === "manifestation"){
        if(this.data.img === 'systems/mta/icons/placeholders/Manifestation.svg') return;
        this.render();
        this.data.img = 'systems/mta/icons/placeholders/Manifestation.svg'; 
      }
      else if(this.data.type === "numen"){
        if(this.data.img === 'systems/mta/icons/placeholders/Numen.svg') return;
        this.render();
        this.data.img = 'systems/mta/icons/placeholders/Numen.svg'; 
      }
      else if(this.data.type === "influence"){
        if(this.data.img === 'systems/mta/icons/placeholders/Influence.svg') return;
        this.render();
        this.data.img = 'systems/mta/icons/placeholders/Influence.svg'; 
      }
      else if(this.data.type === "rite"){
        if(this.data.data.riteType !== "Rite") type = "miracle";
      }
      const path = 'systems/mta/icons/placeholders/';
      
      const placeholders = new Map([
        ["condition", path + 'ConditionTilt.svg'],
        ["tilt", path + 'ConditionTilt.svg'],
        ["environmental", path + 'EnvironmentalTilt.svg'],
        ["spell", path + this.data.data.arcanum + '.svg'],
        ["attainment", path + this.data.data.arcanum + '.svg'],
        ["activeSpell", path + this.data.data.arcanum + '.svg'],
        ["relationship", path + 'Relationship.svg'],
        ["vinculum", path + 'Relationship.svg'],
        ["service", path + 'Service.svg'],
        ["container", path + 'Container.svg'],
        ["merit", path + 'Merit.svg'],
        ["yantra", path + 'Yantra.svg'],
        ["firearm", path + 'Firearm.svg'],
        ["melee", path + 'Melee.svg'],
        ["unarmed", path + 'Unarmed.svg'],
        ["thrown", path + 'Thrown.svg'],
        ["equipment", path + 'Equipment.svg'],
        ["armor", path + 'Armor.svg'],
        ["ammo", path + 'Ammo.svg'],
        ["contract", path + 'Contract.svg'],
        ["pledge", path + 'Pledge.svg'],
        ["devotion", path + 'devotion.svg'],
        ["rite", path + 'Rite.svg'],
        ["miracle", path + 'Miracle.svg'],
        ["discipline_power", path + 'DisciplinePower.svg']
      ]);
      
      img = placeholders.get(type);

      this._data.img = img; 
      this.data.img = img; 
    }
  }

  /* -------------------------------------------- */

  /**
   * Roll the item to Chat, creating a chat card which contains follow up attack or damage roll options
   * @return {Promise}
   */
  async roll() {
      
     // Basic template rendering data
    const token = this.actor.token;
    const templateData = {
      item: this.data,
      actor: this.actor,
      tokenId: token ? `${token.scene._id}.${token.id}` : null,
      isSpell: this.data.type === "spell",
      data: this.getChatData()
    };

    // Render the chat card template
    const template = `systems/mta/templates/chat/item-card.html`;
    const html = await renderTemplate(template, templateData);

    // Basic chat message data
    const chatData = {
      user: game.user._id,
      type: CONST.CHAT_MESSAGE_TYPES.OTHER,
      content: html,
      speaker: ChatMessage.getSpeaker({actor: this.actor, token: this.actor.token}),
      flavor: ""
    };
    
    const actorData = this.actor.data.data;
    
    // Toggle default roll mode
    let rollMode = game.settings.get("core", "rollMode");
    if ( ["gmroll", "blindroll"].includes(rollMode) ) chatData["whisper"] = ChatMessage.getWhisperIDs("GM");
    if ( rollMode === "blindroll" ) chatData["blind"] = true;
    
    let dicePool = 0;
    let flavor = "";
    
    let rollerName = "";
    
    if(this.data.type === "firearm") {
      rollerName = this.name + " - Attack";
      dicePool = actorData.attributes.physical.dexterity.value + actorData.skills.physical.firearms.value;
      flavor = "Dexterity + Firearms";
    }
    else if(this.data.type === "melee") {
      rollerName = this.name + " - Attack";
      if(this.data.data.weaponType === "Unarmed") {
        dicePool = actorData.attributes.physical.strength.value + actorData.skills.physical.brawl.value;
        flavor = "Strength + Brawl";
      }
      else if(this.data.data.weaponType === "Thrown") {
        dicePool = actorData.attributes.physical.dexterity.value + actorData.skills.physical.athletics.value;
        flavor = "Dexterity + Athletics";
      }
      else {
        dicePool = actorData.attributes.physical.strength.value + actorData.skills.physical.weaponry.value;
        flavor = "Strength + Weaponry";
      }
    }
    else if(this.data.type === "influence") {
      rollerName = this.name + " - Influence";
      dicePool = actorData.power.value + actorData.finesse.value;
      flavor = "Power + Finesse";
    }
    else if(this.data.type === "manifestation") {
      rollerName = this.name + " - Manifestation";
      dicePool = actorData.power.value + actorData.finesse.value;
      flavor = "Power + Finesse";
    }
    if(this.data.data.diceBonus) dicePool += this.data.data.diceBonus;

    if((dicePool > 0) || (this.data.type === "equipment") || (this.data.type === "service")){
      let title = "";
      if((this.data.type === "firearm") || (this.data.type === "melee")) title = this.name + " - Attack";
      else title = this.name + " - Skill Check";
      let diceRoller = new DiceRollerDialogue({dicePool: dicePool, flavor: flavor, addBonusFlavor: true, title: title});
      diceRoller.render(true);
    }

    // Create the chat message
    return ChatMessage.create(chatData);
  }
  
  
  
  /**
   * Prepare an object of chat data used to display a card for the Item in the chat log
   * @param {Object} htmlOptions    Options used by the TextEditor.enrichHTML function
   * @return {Object}               An object of chat data to render
   */
  getChatData(htmlOptions) {
    const data = duplicate(this.data.data);
    const labels = this.labels;

    // Rich text description
    data.description = TextEditor.enrichHTML(data.description, htmlOptions);

    // Item type specific properties
    const props = [];
    const fn = this[`_${this.data.type}ChatData`];
    if ( fn ) fn.bind(this)(data, labels, props);

    // General equipment properties
    if ( data.hasOwnProperty("equipped") && !["loot", "tool"].includes(this.data.type) ) {
      props.push(
        data.equipped ? "Equipped" : "Not Equipped"
      );
    }

    // Filter properties and return
    data.properties = props.filter(p => !!p);
    return data;
  }
  
  /* -------------------------------------------- */
  /*  Item Rolls -   */
  /* -------------------------------------------- */

  /**
   * Place an attack roll using an item (weapon, feat, spell, or equipment)
   * Rely upon the Dice5e.d20Roll logic for the core implementation
   *
   * @return {Promise.<Roll>}   A Promise which resolves to the created Roll instance
   */
  async setActive(options={}) {

    const oldSpellData = duplicate(this.data);
    const spellData = mergeObject(oldSpellData, {type: "activeSpell"},{insertKeys: true,overwrite: true,inplace: false,enforceTypes: true});
    const newSpell = await this.actor.createOwnedItem(spellData);
  }
  
  
  /* -------------------------------------------- */
  /*  Chat Message Helpers                        */
  /* -------------------------------------------- */

  static chatListeners(html) {
    html.on('click', '.button', this._onChatCardAction.bind(this));
    html.on('click', '.item-name', this._onChatCardToggleContent.bind(this));
  }
  
  /* -------------------------------------------- */

  /**
   * Handle execution of a chat card action via a click event on one of the card buttons
   * @param {Event} event       The originating click event
   * @returns {Promise}         A promise which resolves once the handler workflow is complete
   * @private
   */
  static async _onChatCardAction(event) {
    event.preventDefault();
      
    // Extract card data
    const button = event.currentTarget;
    button.disabled = true;

    const card = button.closest(".chat-card");
    const messageId = card.closest(".message").dataset.messageId;
    const message =  game.messages.get(messageId);
    const action = button.dataset.action;
    
    console.trace(card)
    
    if(action === "addActiveSpell"){
      // Validate permission to proceed with the roll
      if ( !( game.user.isGM || message.isAuthor ) ) return;

      // Get the Actor from a synthetic Token
      const actor = this._getChatCardActor(card);
      if ( !actor ) return;
      
      //Get spell data
      let description = $(card).find(".card-description");
      description = description[0].innerHTML;
      
      let spellName = $(card).find(".item-name");
      spellName = spellName[0].innerText;
      
      //let image = $(card).find(".item-img");
      //image = image[0].src;
      let image = card.dataset.img;

      let spellFactorsArray = $(card).find(".spell-factors > li");
      spellFactorsArray = $.makeArray(spellFactorsArray);
      spellFactorsArray = spellFactorsArray.map(ele => {
        let text = ele.innerText;
        let advanced = ele.dataset.advanced === "true";
        let splitText = text.split(":",2);
        
        return [splitText[0], splitText[1], advanced];
      });
      let spellFactors = {};
      for(let i = 0; i < spellFactorsArray.length; i++){
        spellFactors[spellFactorsArray[i][0]] = {value: spellFactorsArray[i][1].trim(), isAdvanced: spellFactorsArray[i][2]};
      }
      
      //Special handling for conditional duration, and advanced potency
      let durationSplit = spellFactors.Duration.value.split("(",2);
      spellFactors.Duration.value = durationSplit[0];
      if(durationSplit[1]) spellFactors.Duration.condition = durationSplit[1].split(")",1)[0];
      spellFactors.Potency.value = spellFactors.Potency.value.split("(",1)[0].trim();
      console.trace(spellFactors)
      
      const spellData = {
        name: spellName,
        img: image,
        data: {
          potency: spellFactors.Potency,
          duration: spellFactors.Duration,
          scale: spellFactors.Scale,
          arcanum: card.dataset.arcanum, 
          level: card.dataset.level, 
          practice: card.dataset.practice, 
          primaryFactor: card.dataset.primfactor, 
          withstand: card.dataset.withstand,
          description: description
        }
      };
      
      //Add spell to active spells
      const activeSpellData = mergeObject(spellData, {type: "activeSpell"},{insertKeys: true,overwrite: true,inplace: false,enforceTypes: true});
      const newSpell = await actor.createOwnedItem(activeSpellData);
      ui.notifications.info("Spell added to active spells.");
    }
    
    // Re-enable the button
    button.disabled = false;
  }
  
  /* -------------------------------------------- */

  /**
   * Get the Actor which is the author of a chat card
   * @param {HTMLElement} card    The chat card being used
   * @return {Actor|null}         The Actor entity or null
   * @private
   */
  static _getChatCardActor(card) {

    // Case 1 - a synthetic actor from a Token
    const tokenKey = card.dataset.tokenId;
    if (tokenKey) {
      const [sceneId, tokenId] = tokenKey.split(".");
      const scene = game.scenes.get(sceneId);
      if (!scene) return null;
      const tokenData = scene.getEmbeddedEntity("Token", tokenId);
      if (!tokenData) return null;
      const token = new Token(tokenData);
      return token.actor;
    }

    // Case 2 - use Actor ID directory
    const actorId = card.dataset.actorId;
    return game.actors.get(actorId) || null;
  }
  
  /**
   * Handle toggling the visibility of chat card content when the name is clicked
   * @param {Event} event   The originating click event
   * @private
   */
static _onChatCardToggleContent(event) {
  event.preventDefault();
  const header = event.currentTarget;
  const card = header.closest(".chat-card");
  const content = card.querySelector(".card-description");
  content.style.display = content.style.display === "none" ? "block" : "none";
}
}

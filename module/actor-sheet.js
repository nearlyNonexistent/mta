import {
  DiceRollerDialogue
} from "./dialogue-diceRoller.js";
import {
  ReloadDialogue
} from "./dialogue-reload.js";
import {
  ImprovisedSpellDialogue
} from "./dialogue-improvisedSpell.js";
import {
  createShortActionMessage
} from "./chat.js";
import {
  SkillEditDialogue
} from "./dialogue-skillEdit.js";
import {
  ProgressDialogue
} from "./dialogue-progress.js";
/**
 * Extend the basic ActorSheet with some very simple modifications
 */
export class MtAActorSheet extends ActorSheet {
  constructor(...args) {
    super(...args);

    this._rollParameters = [];
    this._rollDicePool = [];

    Handlebars.registerHelper('isMagCol', function (value) {
      return value === "Magazine";
    });
    Handlebars.registerHelper('isProximiMage', function (value) {
      return value === "Mage" || value === "Proximi";
    });
    Handlebars.registerHelper('isMage', function (value) {
      return value === "Mage";
    });
    Handlebars.registerHelper('isVampire', function (value) {
      return value === "Vampire";
    });
    Handlebars.registerHelper('isChangeling', function (value) {
      return value === "Changeling";
    });
    Handlebars.registerHelper('isEphemeral', function (value) {
      return value === "ephemeral";
    });
    Handlebars.registerHelper('isGhost', function (value) {
      return value === "Ghost";
    });
    Handlebars.registerHelper('isGhostAngel', function (value) {
      return value === "Ghost" || value === "Angel";
    });
    Handlebars.registerHelper('convertVampTouchstone', function (value) {
      let newValues = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1];
      return newValues[value - 1];
    });

    Handlebars.registerHelper('isActiveVampTouchstone', function (value, integrity) {
      let newValues = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1];
      let newValue = newValues[value - 1];
      return newValue <= integrity;
    });

    Handlebars.registerHelper('isActiveTouchstoneChangeling', function (value, composure) {
      return value >= composure + 1;
    });

    Handlebars.registerHelper('convertBool', function (value) {
      return value === true ? "Yes" : "No";
    });
    
    Handlebars.registerHelper('isGoblinContract', function (value) {
      return value === "Goblin";
    });

    Hooks.on("closeProgressDialogue", (app, ele) => {
      if (app === this._progressDialogue) this._progressDialogue = null;
    });

    //TODO: What the fuck is this? Is it even used?
    Handlebars.registerHelper('scaleIndex', function (value) {
      let scaleIndex = CONFIG.MTA.spell_casting.scale.standard.findIndex(v => (v === value));
      if (scaleIndex < 0) scaleIndex = CONFIG.MTA.spell_casting.scale.advanced.findIndex(v => (v === value));
      if (scaleIndex < 0) scaleIndex = 0
      else scaleIndex++;
      return scaleIndex;
    });

  }

  /* -------------------------------------------- */

  /**
   * Get the correct HTML template path to use for rendering this particular sheet
   * @type {String}
   */
  get template() {
    if (!game.user.isGM && this.actor.limited) return "systems/mta/templates/actors/limited-sheet.html";
    if (this.actor.data.type === "ephemeral") return "systems/mta/templates/actors/ephemeral-sheet.html";
    return "systems/mta/templates/actors/character.html";
  }

  /* -------------------------------------------- */

  /**
   * Extend and override the default options used by the 5e Actor Sheet
   * @returns {Object}
   */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["mta-sheet", "worldbuilding", "sheet", "actor"],
      width: 1166,
      height: 830,
      tabs: [{
        navSelector: ".tabs",
        contentSelector: ".sheet-body",
        initial: "attributes"
      }]
    });
  }

  /* -------------------------------------------- */

  /**
   * Prepare data for rendering the Actor sheet
   * The prepared data object contains both the actor data as well as additional sheet options
   */



  getData() {
    const data = super.getData();
    const inventory = {
      firearm: {
        label: "Firearm",
        items: [],
        dataset: ["Dmg.", "Range", "Cartridge", "Magazine", "Init.", "Size"]
      },
      melee: {
        label: "Melee",
        items: [],
        dataset: ["Damage", "Type", "Initiative", "Size"]
      },
      armor: {
        label: "Armor",
        items: [],
        dataset: ["Rating", "Defense", "Speed", "Coverage"]
      },
      equipment: {
        label: "Equipment",
        items: [],
        dataset: ["Dice bonus", "Durability", "Structure", "Size"]
      },
      ammo: {
        label: "Ammo",
        items: [],
        dataset: ["Cartridge", "Quantity"]
      },
      spell: {
        label: "Spells",
        items: [],
        dataset: ["Arcanum", "Level", "Prim. Factor", "Withstand", "Rote Skill"]
      },
      activeSpell: {
        label: "Active Spells",
        items: [],
        dataset: ["Arcanum", "Level", "Potency", "Duration", "Scale"]
      },
      merit: {
        label: "Merits",
        items: [],
        dataset: ["Rating"]
      },
      numen: {
        label: "Numina",
        items: [],
        dataset: ["Reaching"]
      },
      manifestation: {
        label: "Manifestations",
        items: [],
        dataset: []
      },
      influence: {
        label: "Spheres of Influence",
        items: [],
        dataset: ["Rating"]
      },
      condition: {
        label: "Conditions",
        items: [],
        dataset: ["Persistent"]
      },
      tilt: {
        label: "Tilts",
        items: [],
        dataset: ["Environmental"]
      },
      yantra: {
        label: "Yantras",
        items: [],
        dataset: ["Dice Bonus", "Type"]
      },
      attainment: {
        label: "Attainments",
        items: [],
        dataset: ["Arcanum", "Level", "Legacy"]
      },
      relationship: {
        label: "Relationships",
        items: [],
        dataset: ["Impression", "Doors", "Penalty"]
      },
      devotion: {
        label: "Devotions",
        items: [],
        dataset: ["Cost", "Dice Pool", "Action"]
      },
      rite: {
        label: "Rites and Miracles",
        items: [],
        dataset: ["Type", "Target", "Withstand"]
      },
      vinculum: {
        label: "Vinculae",
        items: [],
        dataset: ["Stage"]
      },
      discipline_power: {
        label: "Discipline Powers",
        items: [],
        dataset: ["Discipline", "Level", "Cost", "Dice Pool", "Action"]
      },
      container: {
        label: "Containers",
        items: [],
        dataset: ["Durability", "Structure", "Size"]
      },
      service: {
        label: "Services",
        items: [],
        dataset: ["Dice bonus", "Skill"]
      },
      contract: {
        label: "Contracts",
        items: [],
        dataset: ["Type", "Cost", "Dice Pool", "Action", "Duration"]
      },
      pledge: {
        label: "Pledges",
        items: [],
        dataset: ["Type"]
      }
    };
    if (data.data.characterType === "Changeling") inventory.condition.dataset.push("Clarity");
    data.inventory = inventory;

    data.items.forEach(item => {
      if (data.inventory[item.type]) {
        if (!data.inventory[item.type].items) {
          data.inventory[item.type].items = [];
        }
        data.inventory[item.type].items.push(item);
      }
    });
    data.config = CONFIG.MTA;
    if (data.data.characterType === "Mage" || data.data.characterType === "proximi") data.mana_per_turn = CONFIG.MTA.gnosis_levels[Math.min(9, Math.max(0, data.data.gnosis - 1))].mana_per_turn;
    if (data.data.characterType === "Vampire") data.vitae_per_turn = CONFIG.MTA.bloodPotency_levels[Math.min(10, Math.max(0, data.data.bloodPotency))].vitae_per_turn;
    if (data.data.characterType === "Changeling") data.glamour_per_turn = CONFIG.MTA.glamour_levels[Math.min(9, Math.max(0, data.data.wyrd - 1))].glamour_per_turn;
    if (this.actor.data.type !== "ephemeral") {
      data.data.progress = [{
        name: "INITIAL",
        beats: data.data.beats + 5 * data.data.experience,
        arcaneBeats: data.data.arcaneBeats + 5 * data.data.arcaneExperience
      }].concat(data.data.progress);

      let beats = data.data.progress.reduce((acc, cur) => {
        if (cur && cur.beats) return acc + +cur.beats;
        else return acc;
      }, 0);
      data.beats_computed = beats % 5;
      data.experience_computed = Math.floor(beats / 5);
    }


    if (data.data.characterType === "Mage") {
      let arcaneBeats = data.data.progress.reduce((acc, cur) => {
        if (cur && cur.arcaneBeats) return acc + 1 * cur.arcaneBeats;
        else return acc;
      }, 0);
      data.arcaneBeats_computed = arcaneBeats % 5;
      data.arcaneExperience_computed = Math.floor(arcaneBeats / 5);
    }

    //let updateData = {};
    //if(data.data.health.bashing !== data.data.health.max-data.data.health.value) updateData['data.health.bashing'] = data.data.health.max-data.data.health.value;
    //if (data.data.health.lethal > data.data.health.value) updateData['data.health.lethal'] = data.data.health.value;
    //if (data.data.health.aggravated > data.data.health.value) updateData['data.health.aggravated'] = data.data.health.value;

    //this.actor.update(updateData);

    if (data.data.characterType === "Mage") {
      data.activeSpells = {
        value: data.inventory.activeSpell.items.length,
        max: data.data.gnosis
      };
    }

    if (this.actor.data.type === "ephemeral") {
      if (data.data.rank > 10) data.ephemeralEntityName = "Uber-Entity"
      else if (data.data.rank < 1) data.ephemeralEntityName = "Lesser Entity"
      else data.ephemeralEntityName = CONFIG.MTA.ephemeral_ranks[data.data.rank - 1][data.data.ephemeralType];
    }
    console.trace(data);
    return data;
  }

  /* -------------------------------------------- */

  /**
   * Activate event listeners using the prepared sheet HTML
   * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
   */
  activateListeners(html) {
    super.activateListeners(html);


    //Health tracker
    this._onBoxChange(html);

    html.find('.cell.item-name span').click(event => this._onItemSummary(event));

    //Collapse item table
    html.find('.item-table .cell.header.first').click(event => this._onTableCollapse(event));

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Update Inventory Item
    html.find('.item-edit').click(event => {
      const itemId = event.currentTarget.closest(".item-edit").dataset.itemId;
      const item = this.actor.getOwnedItem(itemId);
      item.sheet.render(true);
    });

    html.find('.item-delete').click(ev => {
      const itemId = event.currentTarget.closest(".item-delete").dataset.itemId;
      this.actor.deleteOwnedItem(itemId);
    });

    // Item Dragging
    let handler = ev => this._onDragStart(ev);
    html.find('.item-row').each((i, li) => {
      if (li.classList.contains("header")) return;
      li.setAttribute("draggable", true);
      li.addEventListener("dragstart", handler, false);
    });

    // Equip Inventory Item
    html.find('.equipped.checkBox input').click(ev => {
      const itemId = event.currentTarget.closest(".equipped.checkBox input").dataset.itemId;
      const item = this.actor.getOwnedItem(itemId);
      let toggle = !item.data.data.equipped;
      const updateData = {
        "data.equipped": toggle
      };
      const updated = item.update(updateData);
    });

    html.find('.spell-rote').click(ev => {
      const itemId = event.currentTarget.closest(".spell-rote").dataset.itemId;
      const item = this.actor.getOwnedItem(itemId);
      let toggle = !item.data.data.isRote
      const updateData = {
        "data.isRote": toggle
      };
      const updated = item.update(updateData);
    });

    html.find('.spell-praxis').click(ev => {
      const itemId = event.currentTarget.closest(".spell-praxis").dataset.itemId;
      const item = this.actor.getOwnedItem(itemId);
      let toggle = !item.data.data.isPraxis
      const updateData = {
        "data.isPraxis": toggle
      };
      const updated = item.update(updateData);
    });

    html.find('.spell-inured').click(ev => {
      const itemId = event.currentTarget.closest(".spell-inured").dataset.itemId;
      const item = this.actor.getOwnedItem(itemId);
      let toggle = !item.data.data.isInured
      const updateData = {
        "data.isInured": toggle
      };
      const updated = item.update(updateData);
    });

    html.find('.item-create').click(this._onItemCreate.bind(this));
    html.find('.discipline-create').click(ev => {
      let ownDisciplines = this.actor.data.data.disciplines.own ? duplicate(this.actor.data.data.disciplines.own) : {};
      let discipline = {
        label: "New Discipline",
        value: 0
      };
      let newKey = Object.keys(ownDisciplines).reduce((acc, ele) => {
        return +ele > acc ? +ele : acc;
      }, 0);
      newKey += 1;
      ownDisciplines[newKey] = discipline;
      let obj = {};
      obj['data.disciplines.own'] = ownDisciplines;
      this.actor.update(obj);
    });

    html.find('.discipline-delete').click(ev => {
      let ownDisciplines = this.actor.data.data.disciplines.own ? duplicate(this.actor.data.data.disciplines.own) : {};
      const discipline_key = event.currentTarget.closest(".discipline-delete").dataset.attribute;

      //delete ownDisciplines[discipline_key];
      ownDisciplines['-=' + discipline_key] = null;
      let obj = {};
      obj['data.disciplines.own'] = ownDisciplines;
      this.actor.update(obj);
    });

    html.find('.discipline-edit').click(ev => {
      const et = $(ev.currentTarget);
      et.siblings(".discipline-name").toggle();
      et.siblings(".attribute-button").toggle();
    });

    // Reload Firearm
    html.find('.item-reload').click(ev => this._onItemReload(ev));

    html.find('.progress').click(async ev => {
      if (this._progressDialogue) this._progressDialogue.bringToTop();
      else this._progressDialogue = await new ProgressDialogue(this.actor).render(true);
    });

    html.find('.item-magValue').mousedown(ev => this._onItemChangeAmmoAmount(ev));

    html.find('.skill-specialty').click(ev => {
      event.preventDefault();
      const skill_key = event.currentTarget.dataset.attribute;
      const skill_type = event.currentTarget.dataset.attributetype;
      new SkillEditDialogue(this.actor, skill_key, skill_type).render(true);
    });


    html.find('.arcana-state').click(ev => {
      const arcanum = event.currentTarget.closest(".arcana-state").dataset.attribute;

      if (Object.keys(this.actor.data.data.arcana.subtle).includes(arcanum)) {
        let updateAttribute = duplicate(this.actor.data.data.arcana.subtle);

        if (this.actor.data.data.arcana.subtle[arcanum].isRuling) {
          updateAttribute[arcanum].isRuling = false;
          updateAttribute[arcanum].isInferior = true;

        } else if (this.actor.data.data.arcana.subtle[arcanum].isInferior) {
          updateAttribute[arcanum].isRuling = false;
          updateAttribute[arcanum].isInferior = false;
        } else {
          updateAttribute[arcanum].isRuling = true;
          updateAttribute[arcanum].isInferior = false;
        }

        let obj = {}
        obj['data.arcana.subtle'] = updateAttribute;
        this.actor.update(obj);
      } else if (Object.keys(this.actor.data.data.arcana.gross).includes(arcanum)) {
        let updateAttribute = duplicate(this.actor.data.data.arcana.gross);

        if (this.actor.data.data.arcana.gross[arcanum].isRuling) {
          updateAttribute[arcanum].isRuling = false;
          updateAttribute[arcanum].isInferior = true;

        } else if (this.actor.data.data.arcana.gross[arcanum].isInferior) {
          updateAttribute[arcanum].isRuling = false;
          updateAttribute[arcanum].isInferior = false;
        } else {
          updateAttribute[arcanum].isRuling = true;
          updateAttribute[arcanum].isInferior = false;
        }

        let obj = {}
        obj['data.arcana.gross'] = updateAttribute;
        this.actor.update(obj);
      }
    });

    // Item Rolling
    html.find('.item-table .item-image').click(event => this._onItemRoll(event));

    // Calculate Max Health
    html.find('.calculate.health').click(event => {
      this.actor.calculateAndSetMaxHealth();
    });

    html.find('.calculate.resource').click(event => {
      this.actor.calculateAndSetMaxResource();
    });

    html.find('.calculate.clarity').click(event => {
      this.actor.calculateAndSetMaxClarity();
    });
    /**
    html.find('.items-subTable .cell.header.first').click(ev => {
      $(this).siblings('.cell:not([header])').hide();
      
    
    });**/




    html.find('.perceptionButton').mousedown(ev => {
      switch (ev.which) {
        case 1:
          this.actor.rollPerception(false, true);
          break;
        case 2:
          break;
        case 3:
          //Quick Roll
          this.actor.rollPerception(true, true);
          break;
      }

    });

    //Clicking roll button
    html.find('.rollButton').mousedown(ev => {
      function ucfirst(s) {
        return s.charAt(0).toLocaleUpperCase() + s.substring(1);
      }

      let attributeChecks = $(".attribute-check:checked");
      this._rollParameters = attributeChecks.toArray().map(v => v.dataset.attributelabel);
      this._rollDicePool = attributeChecks.toArray().map(v => v.dataset.attributevalue);

      let dicepool = this._rollDicePool.reduce((acc, ele) => +acc + +ele, 0);
      let flavor = this._rollParameters.map(x => ucfirst(x.replace(/([^ ])([A-Z])/g, '$1 $2'))).join(' + ');

      for (let i = 0; i < this._rollParameters.length; i++) {
        if (this._rollDicePool[i] < 1) {
          let ele = ucfirst(this._rollParameters[i]);
          if (CONFIG.MTA.skills_physical.includes(ele) || CONFIG.MTA.skills_social.includes(ele)) {
            flavor += " [unskilled]";
            dicepool -= 1;
          } else if (CONFIG.MTA.skills_mental.includes(ele)) {
            flavor += " [unskilled]";
            dicepool -= 3;
          }
        }
      }

      switch (ev.which) {
        case 1:
          let diceRoller = new DiceRollerDialogue({dicePool: dicepool, targetNumber: 8, flavor: flavor, addBonusFlavor: true});
          diceRoller.render(true);
          break;
        case 2:
          break;
        case 3:
          //Quick Roll
          DiceRollerDialogue.rollToChat({
            dicePool: dicepool,
            flavor: flavor
          });
          break;
      }

      //Uncheck attributes/skills and reset
      //let attributeChecks = $(".attribute-check:checked");
      attributeChecks.prop("checked", !attributeChecks.prop("checked"));
      this._rollParameters = [];
      this._rollDicePool = [];
    });

    //Clicking roll button
    html.find('.improvisedSpellButton').mousedown(ev => this._onActivateSpell(ev));
  }

  async _onActivateSpell(ev, spell) {
    const itemData = spell ? duplicate(spell.data.data) : {};
    if (spell) {
      if (spell.data.data.isRote) itemData.castRote = true;
      else if (spell.data.data.isPraxis) itemData.castPraxis = true;
    }

    let activeSpell = new CONFIG.Item.entityClass({
      data: mergeObject(game.system.model.Item["activeSpell"], itemData, {
        inplace: false
      }),
      name: spell ? spell.name : 'Improvised Spell',
      img: spell ? spell.img : ''
    });

    //delete itemData.data["type"];
    //console.trace(itemData);
    //let activeSpell = await this.actor.createOwnedItem(itemData, {temporary: true});
    activeSpell.data.img = itemData.img;

    let spellDialogue = new ImprovisedSpellDialogue(activeSpell, this.actor);
    spellDialogue.render(true);
  }

  async _onItemChangeAmmoAmount(ev) {
    const weaponId = ev.currentTarget.closest(".item-magValue").dataset.itemId;
    const weapon = this.actor.getOwnedItem(weaponId);
    const magazine = weapon.data.data.magazine;

    if (magazine) {
      let ammoCount = magazine.data.data.quantity;

      switch (ev.which) {
        case 1:
          ammoCount = Math.min(ammoCount + 1, weapon.data.data.capacity);
          break;
        case 2:
          break;
        case 3:
          ammoCount = Math.max(ammoCount - 1, 1);
          break;
      }

      this.actor.updateOwnedItem({
        _id: weapon.data._id,
        'data.magazine.data.data.quantity': ammoCount
      });
    }
  }

  /**
   * Handle reloading of a firearm from the Actor Sheet
   * @private
   */
  async _onItemReload(ev) {

    const weaponId = event.currentTarget.closest(".item-reload").dataset.itemId;
    const weapon = this.actor.getOwnedItem(weaponId);
    if (weapon.data.data.magazine) {
      createShortActionMessage("Ejects magazine", this.actor);
      //Eject
      ev.target.classList.remove("reloaded");

      //Add ejected ammo back into the inventory
      const ammoData = weapon.data.data.magazine.data;
      const inventory = this.object.items;

      let foundElement;

      for (let ele of inventory.values()) {
        if ((ele.data.name === ammoData.name) && (ele.data.cartridge === ammoData.cartridge)) {
          foundElement = ele;
        }
      }

      let updateData = [];

      //const index = inventory.findIndex(ele => (ele.data.name === ammoData.name) && (ele.data.cartridge === ammoData.cartridge));
      if (foundElement) updateData.push({
        _id: foundElement.data._id,
        'data.quantity': foundElement.data.data.quantity + ammoData.data.quantity
      }); //Add ammo to existing mag
      else await this.object.createOwnedItem(ammoData); //Add new ammo item

      updateData.push({
        _id: weapon.data._id,
        'data.magazine': null
      }); //Remove mag from weapon
      this.actor.updateOwnedItem(updateData);

    } else {
      //Open reload menu
      let ammoList = new ReloadDialogue(weapon, ev.target);
      ammoList.render(true);
    }
  }

  /**
   * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
   * @param {Event} event   The originating click event
   * @private
   */
  _onItemCreate(event) {
    event.preventDefault();
    const header = event.currentTarget;
    const type = header.dataset.type;
    const itemData = {
      name: `New ${type.capitalize()}`,
      type: type,
      data: duplicate(header.dataset)
    };
    delete itemData.data["type"];
    return this.actor.createEmbeddedEntity("OwnedItem", itemData);
  }



  /**
   * Handle rolling of an item from the Actor sheet, obtaining the Item instance and dispatching to it's roll method
   * @private
   */
  _onItemRoll(event) {
    event.preventDefault();
    const itemId = event.currentTarget.closest(".item-name").dataset.itemId;
    const item = this.actor.getOwnedItem(itemId);



    // Roll spells through the actor
    if (item.data.type === "spell") return this._onActivateSpell(event, item)
    // Otherwise roll the Item directly
    return item.roll();
  }


  _onTableCollapse(event) {
    const et = $(event.currentTarget);
    if (et.hasClass('collapsed')) {
      et.removeClass("collapsed");
      et.parent().siblings('tr').not('.header').show();
    } else {
      et.parent().siblings('tr').not('.header').hide();
      et.addClass("collapsed");
    }

    //$(this).siblings().removeClass("selectedClass"); //remove "selected" class from siblings
    //$(this).addClass("selectedClass"); //add selected class to clicked element

  }

  _onItemSummary(event) {
    event.preventDefault();
    let li = $(event.currentTarget).parents(".item-row"),
      item = this.actor.getOwnedItem(li.data("item-id")),
      chatData = item.getChatData({
        secrets: this.actor.owner
      }),
      tb = $(event.currentTarget).parents(".item-table");

    let colSpanMax = [...tb.get(0).rows[0].cells].reduce((a, v) => (v.colSpan) ? a + v.colSpan * 1 : a + 1, 0);

    // Toggle summary
    if (li.hasClass("expanded")) {
      let summary = li.next(".item-summary");
      summary.children().children("div").slideUp(200, () => summary.remove());
    } else {
      let tr = $(`<tr class="item-summary"> <td colspan="${colSpanMax}"> <div> ${chatData.description} </div> </td> </tr>`);
      //let props = $(`<div class="item-properties"></div>`);
      //chatData.properties.forEach(p => props.append(`<span class="tag">${p}</span>`));
      //div.append(props);
      let div = tr.children().children("div");
      div.hide();
      li.after(tr);
      div.slideDown(200);
    }
    li.toggleClass("expanded");
  }


  _onBoxChange(html) {
    let parent = html.find('.kMageTracker');
    parent.toArray().forEach(tracker => {
      if (!tracker.dataset.active) {
        let td = tracker.dataset;
        let trackerName = td.name || 'unknowntracker';
        let trackerNameDelimiter = '.';
        let tType = (td.type) ? td.type : 'oneState';
        let stateOrder = (td.states) ? td.states.split('/') : ['off', 'on'];
        let initialStateCount = stateOrder.map(v => (td[v]) ? td[v] * 1 : 0).map((v, k, a) => ((k > 0) ? a[0] - v : v)).map(v => (v < 0) ? 0 : v); //let initialStateCount = stateOrder.map( v => (td[v])?td[v]*1:0 );
        let limit = stateOrder.length - 1;
        let dots = [...Array(initialStateCount[0])].map(v => 0);
        let extraContent = () => '';
        let ex = {};
        tracker.dataset.active = 1;
        let renderBox = tracker.appendChild(document.createElement('div'));
        let inputs = stateOrder.map((v, k) => {
          if (k === 0) {
            tracker.insertAdjacentHTML('afterbegin', `<div class="niceNumber" onpointerdown="let v = event.target.textContent;let i= this.querySelector('input');if(v=='+'){i.value++}if(v=='−'){i.value--};i.dispatchEvent(new Event('input',{'bubbles':true}));">
               <input name="${trackerName + trackerNameDelimiter + v}" type="number" value="${initialStateCount[k]}">
               <div class="numBtns"><div class="plusBtn">+</div><div class="minusBtn">−</div></div>
              </div>`);
            return tracker.firstChild;
          } else {
            let i = document.createElement('input');
            i.type = 'hidden';
            i.name = trackerName + trackerNameDelimiter + v
            i.value = initialStateCount[0] - initialStateCount[k]; //i.value = initialStateCount[k];
            return tracker.insertAdjacentElement('afterbegin', i);
          }
        });

        initialStateCount.slice(1).forEach((sCount, stateN) => {
          let stateNum = stateN + 1;
            [...Array(Math.min(sCount, dots.length))].forEach((v, k) => dots[k] = stateNum);
        });

        let renderBoxes = () => {
          renderBox.innerHTML =
            '<div class="boxes">' + dots.reduce((a, v, k) => a + `<div data-state="${v}" data-index="${k}"></div>`, '') + '</div>' + extraContent();
        };

        let updateDots = (num = false, index = false) => {
          let abNum = Math.abs(num);
          if (index) {
            if (num > 0) {
              dots = dots.map((dot, i) => (dot < num && i <= index) ? num : dot)
            } else if (num < 0) {
              dots = dots.map((dot, i) => (dot <= abNum && dot > 0 && i >= index) ? 0 : dot)
            }
          } else {
            if (num > 0) {
              let updateIndex = dots.findIndex(v => (v < num));
              if (updateIndex > -1) {
                dots[updateIndex] = num;
              } else {
                if (num < limit) {
                  updateDots(num * 1 + 1); //Never gets called
                }
              }
            } else if (num < 0) {
              //let updateIndex = dots.reduce( (a,v,k) => (v == (num*-1) && v > 0)?k:a, -1 );
              let updateIndex = dots.findIndex(v => (v <= abNum && v > 0));

              if (updateIndex > -1) {
                dots[updateIndex] = 0; // dots.map( (v,k) => (k >= updateIndex)?0:v );
              }
            }
          }
          dots.sort().reverse();

          // Special states
          if (tType == 'health') {
            ex.dicePenalty = -1 * dots.slice(-limit).reduce((a, v) => (v > 0) ? a + 1 : a, 0);
            ex.penaltyState = dots[dots.length - 1];
          }
          
          if (tType == 'clarity') {
            const dmg = dots.filter(v => v === 0);
            ex.dicePenalty = (dmg.length < 3) ? -2 : (dmg.length < 5) ? -1 : 0;
            if(dmg.length === dots.length) ex.dicePenalty += 2;
            if(ex.dicePenalty >= 0) ex.dicePenalty = '+' + ex.dicePenalty;
            if(dmg.length === 0) ex.dicePenalty = '?';
          }

          // update inputs
          stateOrder.forEach((state, stateNum) => {
            let sCount = dots.length - dots.reduce((a, d) => (d >= stateNum) ? ++a : a, 0); //let sCount = dots.reduce((a, d) => (d >= stateNum) ? ++a : a, 0);
            let iName = trackerName + trackerNameDelimiter + state;
            let i = inputs.find(v => (v.name && v.name == iName));
            let ele = inputs.find(v => (v.name && v.name == iName));
            let oldValue = (ele) ? +ele.value * 1 : false;
            if (oldValue !== false && oldValue != sCount) {
              //if(iName === "data.health.bashing") this.actor.update({'data.health.value': this.actor.data.data.health.max - sCount});
              i.value = sCount;
              /**
                            i.dispatchEvent(new Event('input', {
                              'bubbles': true
                            }));**/
            }
          });
          //inputs[0].dispatchEvent(new Event('change',{'bubbles':true}));
          renderBoxes();
          if (num !== false) {
            parent.submit();
          }
        };

        if (tType == 'health' && !(this.actor.data.type === "ephemeral")) {
          ex.dicePenalty = 0;
          ex.penaltyState = 0;
          ex.penaltyMap = {
            '0': 'physically stable',
            '1': 'losing consciousness',
            '2': 'bleeding out',
            '3': 'dead'
          };

          extraContent = () => `<div class="info"><span>You are <b>${ex.penaltyMap[ex.penaltyState]}</b>.</span><span>Dice Penalty:  <b>−${Math.abs(ex.dicePenalty)}</b></span></div>`;
        }
        
        if (tType == 'clarity') {
          ex.dicePenalty = 0;
          ex.penaltyState = 0;
          ex.penaltyMap = {
            '+2': 'lucid',
            '+1': 'rational',
            '+0': 'clear-headed',
            '-1': 'hazy',
            '-2': 'loosing track',
            '?': 'in a coma'
          };

          extraContent = () => `<div class="info"><span>You are <b>${ex.penaltyMap[ex.dicePenalty]}</b>.</span><span>Perception:  <b>${ex.dicePenalty}</b></span></div>`;
        }

        if (this.options.editable) {
          tracker.addEventListener('pointerdown', (e, t = e.target) => {
            if (t.dataset.state) {
              let s = t.dataset.state * 1;
              let n = s;
              let index = false;
              if (e.button === 2) {
                n = -s;
              } else {
                n = s + 1;
              }
              if (tType == 'oneState' && t.dataset.index) {
                index = t.dataset.index;
              }
              updateDots((n > limit) ? -limit : n, index);
              //parent.submit();
            }
          });
          tracker.addEventListener('input', (e, t = e.target) => {
            if (t.type == 'number') {
              let nl = +t.value * 1;
              if (nl < dots.length) {
                dots = dots.slice(0, +t.value * 1);
              } else if (nl > dots.length) {
                dots.push(...[...Array(nl - dots.length)].map(v => 0));
              }

              updateDots(0);
            }
          });
        }
        updateDots();
        tracker.dataset.active = 1;
      }
    });
  }

  /** @override */
  async _updateObject(event, formData) {
    const data = this.getData();

    // TODO: This can be removed once 0.7.x is release channel
    if (!formData.data) formData = expandObject(formData);
    if (formData.data?.characterType === "Changeling") {
      //Adjust the number of touchstones on clarity update
      let touchstones = formData.data.touchstones_changeling ? duplicate(formData.data.touchstones_changeling) : {};
      let touchstone_amount = Object.keys(touchstones).length;
      const clarity_max = formData.data.clarity?.max ? formData.data.clarity.max : data.data.clarity.max;
        
      if (touchstone_amount < clarity_max) {
        while (touchstone_amount < clarity_max) {
          touchstones[touchstone_amount + 1] = "";
          touchstone_amount++;
        }
      } else if (touchstone_amount > clarity_max) {
        while (touchstone_amount > clarity_max) {
          touchstones['-=' + touchstone_amount] = null;
          touchstone_amount -= 1;
        }
      }
        formData.data.touchstones_changeling = touchstones;
    }


    // Update the Item
    super._updateObject(event, formData);
  }

}